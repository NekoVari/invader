#include <time.h>
#include <iostream>
#include <stdio.h>
#include <curses.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

void SetCursorPosition(int XPos, int YPos) {
    printf("\033[%d;%dH",YPos+1,XPos+1);
}
//void getCursor(int* x, int* y) {
//   printf("\033[6n");  /* This escape sequence !writes! the current
//                          coordinates to the terminal.
//                          We then have to read it from there, see [4,5].
//                          Needs <termios.h>,<unistd.h> and some others */
//   scanf("\033[%d;%dR", x, y);
//}
void drawboat(int *h,int *i){
    printf("\033[41m\033[1;32m <-Y-> \033[0m");
}
void drawvoid(){
    printf("\033[41m       ");
}
int A(int *i,int *h){
    if(*i>0){
            SetCursorPosition(--*i,*h);
            }
            return 0;
}
int D(int *i,int *h){
    if(*i<231){
         SetCursorPosition(++*i,*h);
    }
    return 0;
}
int W(int *i,int *h){
    if(*h>0){
            SetCursorPosition(*i,*h);
            drawvoid();
            SetCursorPosition(*i,--*h);
            }
            return 0;
}
int S(int *i,int *h){
    if(*h<58){
        SetCursorPosition(*i,*h);
        drawvoid();
        SetCursorPosition(*i,++*h);
    }
    return 0;
}
int main () {
    //system("COLOR 12");
    //system("COLOR [attr]");
    //start_color();
    //init_pair(1,COLOR_BLUE, COLOR_RED);
    //wbkgd(stdscr, COLOR_PAIR(1));

    clock_t start_t,this_t;
    start_t=clock();
    int i=10,h=10,*ip,*hp,bli[5]={-1,-1,-1,-1,-1},blh[5]={-1,-1,-1,-1,-1},shoot=0,j,temp,ft=0;
    ip=&i;
    hp=&h;
    char inp,sett;
    initscr();
    cbreak();
    noecho();
    scrollok(stdscr, TRUE);
    nodelay(stdscr, TRUE);
    sleep(1);//รอโปรแกรมเปิด

    start_color();
    init_pair(1,COLOR_BLUE, COLOR_RED);
    wbkgd(stdscr, COLOR_PAIR(1));

    while(1){
        this_t=clock();
        SetCursorPosition(i,h);
        printf("\e[?25l");//ปิดcursor
        if(ft<1){//สร้างเรือรอบแรกสุด
            drawboat(hp,ip);
            ft++;
        }
        inp=getch();//รับจากแป้นพิมพ์
        if(inp=='a'){
            A(ip,hp);
            drawboat(hp,ip);
            sett='a';
        }
        else if(inp=='d'){
            D(ip,hp);
            drawboat(hp,ip);
            sett='d';
        }
        else if(inp=='w'){
            W(ip,hp);
            drawboat(hp,ip);
            sett='w';
        }
        else if(inp=='s'){
            //S(ip,hp);
            //drawboat();
            sett='0';
        }
        if(inp==' '){//ยิง
            if(shoot>4){//ยิงได้นัดวนๆ
                shoot=0;
            }
            if(blh[shoot]==-1){//ยิง
                bli[shoot]=i+3;
                blh[shoot]=h-1;
                if(blh[shoot]>0){
                    SetCursorPosition(bli[shoot],blh[shoot]);
                    printf("\033[41m\033[1;33m|\033[0m");
                }
                shoot++;
            }
        }
        if((this_t-start_t)/CLOCKS_PER_SEC==1){//วนรูปทุกๆ1วิ
            for(j=0;j<5;j++){//เช็คกระสุน
                if(blh[j]!=-1){
                    if(blh[j]==0){//ยิงออกนอกจอ
                        SetCursorPosition(bli[j],blh[j]);
                        printf("\033[41m ");
                        //attron(COLOR_PAIR(3));
                        //mvaddch(blh[j], bli[j],' ');
                        //attroff(COLOR_PAIR(3));

                        blh[j]=-1;
                        bli[j]=-1;
                    }
                    else if(blh[j]>0){//กระสุนเคลื่อนที่
                        SetCursorPosition(bli[j],blh[j]);
                        printf("\033[41m ");
                        SetCursorPosition(bli[j],--blh[j]);
                        printf("\033[41m\033[1;33m|\033[0m");
                        //attron(COLOR_PAIR(3));
                        //mvaddch(blh[j], bli[j],' ');
                        //mvaddch(--blh[j], bli[j],'|');
                        //attroff(COLOR_PAIR(3));
                    }
                }
            }
            if(sett=='a'){//เรือขยับ
                A(ip,hp);
                drawboat(hp,ip);
            }
            else if(sett=='d'){
                D(ip,hp);
                drawboat(hp,ip);
            }
            else if(sett=='w'){
                W(ip,hp);
                drawboat(hp,ip);
            }
            else if(sett=='s'){
                S(ip,hp);
                drawboat(hp,ip);
            }
            start_t=this_t;
        }
    }
    return(0);
}
